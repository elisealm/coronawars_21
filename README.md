# CoronaWars

CoronaWars is a game where the goal is to infect an opponent. Use the left and right buttons to move your player horizontally and shoot a coronavirus by clicking the shoot button. Each time you hit the opponent, your score increases by 10 points. The game also includes power-ups, that can help you increase your score faster. The game can be played with a friend as many times as you want - maybe one of you will set the highest score on the high score list?

Link to youtube demonstration of the game: [https://youtu.be/PkcqwjGeb7M](https://youtu.be/PkcqwjGeb7M).


### Developers 
* Elise Almestad 
* Elin Schanke Funnemark 
* Emma Valen Rian 
* Vår Sørensen Sæbøe-Larssen
* Oline Vikøren Zachariassen 

## Installation
### Clone repository:
> git clone: https://gitlab.stud.idi.ntnu.no/elisealm/coronawars_21.git 

Open Android Studio and choose file -> open...
Navigate among your files and then choose the project that you cloned. Wait for the gradle to finish building and then the application is ready to run. 

### Compiling and running: 

#### Running on an Android device
* Connect your Android device via USB to the computer. Make sure your API level is 21 or higher and that the android device is 64bit. This is common among new Android devices.
* Allow permissions on your chosen Android device, so that the computer can access the device. 
* Enter Android Studio, and in the top bar, you will find available android devices that can run the application. 
* Choose your connected Android device
* Press the play button, and you are ready to play the game!

![](https://i.imgur.com/8EOZ0f9.png)


#### Running on an android emulator
* Open Android Studio and click on *Tools* in the top bar. From there, open *AVD Manager*. Here you get an overview of the Android virtual devices. Choose one of the devices and press the pen icon to edit the device.

![](https://i.imgur.com/wuIHNND.png)

* Make sure the API is set to *x86* and press *Finish*. Now exit the *AVD Manager*. 
* Make sure the downloaded Android device is set in the top bar of your IDE.
* Press the play button next to your device, and start playing the game! 

## Use the application
This section will guide you through how to use the application and play the game.

### Main menu 
When you open the application, you enter the main menu, as shown in the figure below. From here you can press *High score*-button, *Play Game*-button, *Settings*-button, and *Tutorial*-button which will send you to the corresponding screens.

![](https://i.imgur.com/zTd1FZq.png)


### Tutorial
The tutorial, as shown in the figure below, show users how to play the game by scrolling through a guide which demonstrates the game.

![](https://i.imgur.com/D2NzB1O.png)


### Settings
In the settings screen, as shown in the figure below, the user can change the background color and turn the sound on/off by pressing the *Color*- and *Sound*-buttons.

![](https://i.imgur.com/HKfNSC8.png)

### Play Game 
When pressing *Play Game*, the game starts with a countdown of 3 seconds.

Our game is turn-based, and player 1 plays the first round. After 30 seconds, it is player 2's turn. The main goal of the game is to get the highest score. To gain points, the player must hit the target on the opposite side. The player can also gain more points by hitting the power-ups that show up on the screen at random times during the game. More details about the game can be read about in the tutorial. 

During the game, if a player wants to quit, they can press the *Quit Game*- button, which guide them back the Main menu screen.

When both players have played their turn, it is possible for them to add their result to the high score board, as shown in the figure below, by adding their name and pressing *Add high score*. The high score board is sorted by the highest score. Pressing the *High score*-button or the *Main menu*-button will guide the players to the High Score screen or the Main Menu screen.

![](https://i.imgur.com/1g6I41k.png)


### High Score 
The high score screen, as shown in the figure below, presents the high scores added by different players. This board is online, which means that high scores from players across devices will show up. 

![](https://i.imgur.com/1yTJOp6.png)


## Technology and frameworks

#### [Android Studio](https://developer.android.com/studio)
#### [LibGDX](https://libgdx.com/)
#### [Ashley ECS](https://github.com/libgdx/ashley/wiki)
#### [Firebase](https://firebase.google.com/docs/database)

## Developers 
#### Elise Almestad 
#### Elin Schanke Funnemark 
#### Emma Valen Rian 
#### Vår Sørensen Sæbøe-Larssen
#### Oline Vikøren Zachariassen 



