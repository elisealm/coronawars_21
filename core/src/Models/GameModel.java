package Models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.TimeUtils;

public class GameModel {
    public int period;
    public long startTime;
    public long elapsedTime;
    private int score1;
    private int score2;
    private boolean shootButtonBeenClicked;

    private TextureAtlas playerAtlas;
    private Sprite playerOneText;
    private Sprite playerTwoText;

    public GameModel(){
        this.startTime= TimeUtils.millis();
        this.elapsedTime = TimeUtils.timeSinceMillis(startTime);
        this.period = 10;
        this.score1 = 0;
        this.score2 = 0;

        this.playerAtlas = new TextureAtlas("save_score_sprites.txt");
        this.playerOneText = playerAtlas.createSprite("player1");
        this.playerTwoText = playerAtlas.createSprite("player2");

    }

    public Sprite getPlayerOneText(){return this.playerOneText;}
    public Sprite getPlayerTwoText(){return this.playerTwoText;}

    public boolean isShootButtonBeenClicked() {
        return shootButtonBeenClicked;
    }

    public void setShootButtonBeenClicked(boolean shootButtonBeenClicked) {
        this.shootButtonBeenClicked = shootButtonBeenClicked;
    }

    public int getScore1(){return this.score1;}
    public int getScore2(){return this.score2;}
    public void setScore1(int score){score1 = score;}
    public void setScore2(int score){score2 = score;}






}
