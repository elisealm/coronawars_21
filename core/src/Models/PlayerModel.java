package Models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;


public class PlayerModel {
    public int position_x;
    public int position_y;
    public int life;
    private TextureAtlas textureAtlas;
    public Sprite sprite;


    public PlayerModel( int position_x, int position_y, int life){
        this.position_x = position_x;
        this.position_y = position_y;
        this.life = life;
        this.textureAtlas = new TextureAtlas("misc_sprites.txt");
        this.sprite = textureAtlas.createSprite("shooter1x");

    }
    public int getPosition_x() {
        return this.position_x;
    }
    public int getPosition_y() {
        return this.position_y;
    }

}
