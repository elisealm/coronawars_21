package Models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class VirusModel {
    public int pos_x;
    public int pos_y;
    private TextureAtlas textureAtlas;
    private Sprite sprite;


    public VirusModel(int pos_x, int pos_y){
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.textureAtlas = new TextureAtlas("covid_sprites.txt");
        this.sprite = textureAtlas.createSprite("green_virus");
    }

    public void setSprite(Sprite sprite){this.sprite = sprite;}
    public Sprite getSprite(){return this.sprite;}
}
