package Models.Components;

import com.badlogic.ashley.core.Component;

public class PositionComponent implements Component {
    public int pos_x;
    public int pos_y;
    public boolean showing;
}
