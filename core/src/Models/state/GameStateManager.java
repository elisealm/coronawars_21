package Models.state;

import java.util.Stack;

public class GameStateManager {

    public Stack<IState> states;

    public GameStateManager(){
        this.states = new Stack<>();
    }
    public void push(IState state){

        states.push(state);
    }

    public void pop(){

        states.pop();
    }

    public void changeState(IState state){
        pop();
        states.push(state);

    }

    public IState currentState() {
        return states.peek();
    }

}
