package Models.state;

public interface IState {
    void changeState (IState state);
    void changeView();
    void reset();
}
