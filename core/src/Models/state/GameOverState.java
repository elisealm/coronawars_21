package Models.state;

public class GameOverState implements IState {
    private GameStateManager gsm;

    public GameOverState(GameStateManager gsm){
        this.gsm = gsm;

    }

    @Override
    public void changeState(IState state) {
        // Change back to player 1 state - ready for a new game
        gsm.changeState(state);
    }

    @Override
    public void changeView() {
        // Should not change view automatically from GameOverState
    }

    @Override
    public void reset() {
        // reset to default player one state
        gsm.changeState(new PrePlayerOneState(gsm));
    }
}
