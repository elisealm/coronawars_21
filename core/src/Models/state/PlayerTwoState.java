package Models.state;

import com.mygdx.game.CoronaWars;

import Views.GameOverView;

public class PlayerTwoState implements IState {

    private GameStateManager gsm;

    public PlayerTwoState(GameStateManager gsm){
        this.gsm = gsm;
    }


    @Override
    public void changeState(IState state) {
        this.changeView();
        gsm.changeState(state);
    }


    @Override
    public void changeView() {
        // Should go to GameOverView at the end of PlayerTwoState
        CoronaWars.getInstance().setScreen(new GameOverView());

    }

    @Override
    public void reset() {
        // reset to default (pre player one) state
        gsm.changeState(new PrePlayerOneState(gsm));
    }

}
