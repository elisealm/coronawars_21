package Models.state;

import com.mygdx.game.CoronaWars;

import Views.GameView;

public class PrePlayerTwoState implements IState{

    private GameStateManager gsm;

    public PrePlayerTwoState(GameStateManager gsm){
        this.gsm = gsm;
    }


    @Override
    public void changeState(IState state) {
        this.changeView();
        gsm.changeState(state);
    }

    @Override
    public void changeView() {
        CoronaWars.getInstance().setScreen(new GameView());
    }

    @Override
    public void reset() {
        // reset to default (pre player one) state
        gsm.changeState(new PrePlayerOneState(gsm));
    }
}
