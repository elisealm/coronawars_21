package Models.state;

import com.mygdx.game.CoronaWars;
import Views.PreGameView;

public class PlayerOneState implements IState {

    private GameStateManager gsm;

    public PlayerOneState(GameStateManager gsm){
        this.gsm = gsm;
    }


    @Override
    public void changeState(IState state) {
        this.changeView();
        gsm.changeState(state);
    }


    @Override
    public void changeView() {
        // Should go to new game view (for player two) at the end of PlayerOneState
        CoronaWars.getInstance().setScreen(new PreGameView());
    }

    @Override
    public void reset() {
        // reset to default (pre player one) state
        gsm.changeState(new PrePlayerOneState(gsm));
    }



}
