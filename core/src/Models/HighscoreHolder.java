package Models;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class HighscoreHolder {

    private Map<String, Long> scores = new LinkedHashMap<>();

    long score;
    String name;

    public HighscoreHolder() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public Map<String, Long> getScoresDescending() {
        // New Map descending_scores will contain scores in descending order
        Map<String, Long> descending_scores = scores.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

        return descending_scores;
    }

    public void setNewScore(String name, long score) {
        scores.put(name, score);
    }

}
