package Models;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import Controllers.PowerUpSystem;
import Models.Components.LifeTimeComponent;
import Models.Components.PositionComponent;
import Models.Components.SpriteComponent;
import Models.Components.VelocityComponent;

public class PowerUp {

    private Engine engine;
    private Sprite sprite1;
    private Sprite sprite2;
    private TextureAtlas textureAtlas;

    public PowerUp(Engine engine){
        this.engine = engine;
        this.textureAtlas = new TextureAtlas("covid_sprites.txt");
        this.sprite1 = textureAtlas.createSprite("face_mask");
        this.sprite2 = textureAtlas.createSprite("mutant");

    }

    public Entity createPowerUp(){
        //Create powerUp
        Entity powerUpEntity = new Entity();
        powerUpEntity.add(new PositionComponent());
        powerUpEntity.add(new LifeTimeComponent());
        powerUpEntity.add(new SpriteComponent());

        //connect powerUp to engine and set initial values
        engine.addEntity(powerUpEntity);
        engine.getSystem(PowerUpSystem.class).setLifeTime(powerUpEntity, 4);
        engine.getSystem(PowerUpSystem.class).setShowTime(powerUpEntity);
        engine.getSystem(PowerUpSystem.class).setPosition(powerUpEntity);
        engine.getSystem(PowerUpSystem.class).setShowing(powerUpEntity, false);
        engine.getSystem(PowerUpSystem.class).setSprite(powerUpEntity, sprite1);

        return powerUpEntity;
    }

    public Entity createSuperPowerUp(){
        //Create superPowerUp
        Entity superPowerUpEntity = new Entity();
        superPowerUpEntity.add(new PositionComponent());
        superPowerUpEntity.add(new LifeTimeComponent());
        superPowerUpEntity.add(new VelocityComponent());
        superPowerUpEntity.add(new SpriteComponent());

        //connect powerUp to engine and set initial values
        engine.addEntity(superPowerUpEntity);
        engine.getSystem(PowerUpSystem.class).setLifeTime(superPowerUpEntity, 4);
        engine.getSystem(PowerUpSystem.class).setShowTime(superPowerUpEntity);
        engine.getSystem(PowerUpSystem.class).setPosition(superPowerUpEntity);
        engine.getSystem(PowerUpSystem.class).setShowing(superPowerUpEntity, false);;
        engine.getSystem(PowerUpSystem.class).setVelocity(superPowerUpEntity);
        engine.getSystem(PowerUpSystem.class).setSprite(superPowerUpEntity, sprite2);
        return superPowerUpEntity;
    }





}
