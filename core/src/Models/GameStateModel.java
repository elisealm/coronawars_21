package Models;

public class GameStateModel {

    private boolean gameStarted;
    private long elapsedTime;
    private long timeLeft;

    public long getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(long timeLeft) {
        this.timeLeft = timeLeft;
    }

    public GameStateModel(boolean gameStarted, long elapsedTime, long timeLeft) {
        this.gameStarted = gameStarted;
        this.elapsedTime = elapsedTime;
        this.timeLeft = timeLeft;
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
}
