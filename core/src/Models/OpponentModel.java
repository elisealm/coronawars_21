package Models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class OpponentModel {

    public int position_x;
    public int position_y;
    private TextureAtlas textureAtlas;
    public Sprite sprite;
    public float height;
    public float width;

    public OpponentModel(){
        this.textureAtlas = new TextureAtlas("misc_sprites.txt");
        this.sprite = textureAtlas.createSprite("target1x");
        this.position_x = (int)(Gdx.graphics.getWidth()/2-(sprite.getWidth()/2));
        this.position_y = (int) (Gdx.graphics.getHeight()-(sprite.getHeight())- 100);
        this.height = sprite.getHeight();
        this.width = sprite.getWidth();

    }
}
