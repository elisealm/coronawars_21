package Models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class TutorialModel {

        private Table container;
        private Table table;
        private ImageButton imageButton;


        public TutorialModel(){
            container = new Table();
            table = new Table();
        }

        public Table getContainer() {
            return this.container;
        };
        public Table getTable() {
            return this.table;
        };



        public void addImage(Texture image){
            imageButton = new ImageButton(new Image(image).getDrawable());
            table.add(imageButton);
        }
}
