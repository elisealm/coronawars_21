package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import Controllers.PlayerController;

public class MovementButton {

    private float x, y;
    private Button button;
    private Sprite buttonSprite;
    private PlayerController player;
    private String direction;

    public Button getButton() {
        return button;
    }

    public MovementButton(Sprite buttonSprite, float x, float y, final PlayerController player, final String direction) {
        this.buttonSprite = buttonSprite;
        this.x = x;
        this.y = y;
        this.button = new Button(new SpriteDrawable(buttonSprite));
        this.player = player;
        this.direction = direction;

        button.setPosition(x, y);

        button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float xpos, float ypos, int pointer, int button) {
                if (direction.equals("left")) {
                    player.leftDown = true;
                    //player.moveLeft();
                } else if (direction.equals("right")) {
                    player.rightDown = true;
                    //player.moveRight();
                }
                return true;
            }
            public void touchUp(InputEvent event, float xpos, float ypos, int pointer, int button) {
                if (direction.equals("left")) {
                    player.leftDown = false;
                } else if (direction.equals("right")) {
                    player.rightDown = false;
                }
            }
        });
    }

}
