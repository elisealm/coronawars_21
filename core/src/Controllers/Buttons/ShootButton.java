package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import Controllers.GameController;
import Controllers.PlayerController;

public class ShootButton {

    private float x, y;
    private Button button;
    private Sprite buttonSprite;
    private GameController gameController;
    private PlayerController player;
    private String direction;
    private Drawable drawable;

    public Button getButton() {
        return button;
    }

    public ShootButton(Sprite buttonSprite, float x, float y, final PlayerController player, final GameController gameController, final String direction) {
        this.buttonSprite = buttonSprite;
        this.x = x;
        this.y = y;
        this.gameController = gameController;
        this.player = player;
        this.direction = direction;

        this.drawable = new TextureRegionDrawable(new TextureRegion(buttonSprite));
        this.button = new ImageButton(drawable);
        button.setPosition(x, y);

        button.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent inputEvent, float xpos, float ypos) {

                if (direction.equals("left")) {
                    gameController.gameModel.setShootButtonBeenClicked(true);
                    gameController.virus(player.getPlayer().getPosition_x() - 40, player.getPlayer().getPosition_y() + 140);
                } else if (direction.equals("right")) {
                    gameController.gameModel.setShootButtonBeenClicked(true);
                    gameController.virus(player.getPlayer().getPosition_x() + 210, player.getPlayer().getPosition_y() + 140);
                }
            }

        });
    }

}
