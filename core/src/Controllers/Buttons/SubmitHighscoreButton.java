package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.game.CoronaWars;




public class SubmitHighscoreButton {
    private String color;
    private float x;
    private float y;
    private Button button;
    private Sprite buttonSprite;
    public CoronaWars game;
    private String name;
    private int score;
    private boolean IsSubmit;

    public Button getButton() {
        return button;
    }

    public boolean isSubmit() {
        return IsSubmit;
    }


    public SubmitHighscoreButton(Sprite buttonSprite, float x, float y ) {
        this.buttonSprite = buttonSprite;
        this.x = x;
        this.y = y;
        this.button = new Button(new SpriteDrawable(buttonSprite));
        button.setPosition(x, y);
        this.name = name;
        this.score = score;
        this.game = game;
        this.IsSubmit  = false;

        button.addListener(new ClickListener() {
           @Override
           public void clicked(InputEvent inputEvent, float xpos, float ypos) {
               IsSubmit = true;
           }
        }
        );


}
}

