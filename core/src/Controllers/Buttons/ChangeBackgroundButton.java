package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import Controllers.PlayerController;
import Controllers.SettingsController;



public class ChangeBackgroundButton {
    private String color;
    private float x;
    private float y;
    private Button button;
    private Sprite buttonSprite;
    private SettingsController settings;


    public Button getButton() {
        return button;
    }

    public ChangeBackgroundButton(Sprite buttonSprite, float x, float y, final String color, final SettingsController settings) {
        this.buttonSprite = buttonSprite;
        this.x = x;
        this.y = y;
        this.button = new Button(new SpriteDrawable(buttonSprite));
        button.setPosition(x, y);
        this.color = color;
        this.settings = settings;



        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent inputEvent, float xpos, float ypos) {
                if (color.equals("brown")) {
                    settings.changeBackground("brown");
                } else if (color.equals("purple")) {
                    settings.changeBackground("purple");
                } else if (color.equals("green")) {
                    settings.changeBackground("green");
                }
            }
        });
    }


}
