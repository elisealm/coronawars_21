package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.game.CoronaWars;

import Views.HighScoreView;
import Views.MenuView;
import Views.PreGameView;
import Views.SettingsView;
import Views.TutorialView;
import Views.GameOverView;

public class ChangeViewButton {

    private float x, y;
    private Button button;
    private Sprite buttonSprite;
    private String screen;

    public Button getButton() {
        return button;
    }

    public ChangeViewButton(Sprite buttonSprite, final String screen, float x, float y) {
        this.buttonSprite = buttonSprite;
        this.screen = screen;
        this.x = x;
        this.y = y;
        this.button = new Button(new SpriteDrawable(buttonSprite));

        button.setPosition(x, y);

        // Change view depending on string screen when clicked
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent inputEvent, float xpos, float ypos) {

                if(screen.equals("GAME")) {
                    CoronaWars.getInstance().setScreen(new PreGameView());
                    CoronaWars.getInstance().startGame();
                }
                else if(screen.equals("HIGHSCORE")){
                    CoronaWars.getInstance().setScreen(new HighScoreView());
                }
                else if(screen.equals("QUIT")) {
                    // Go to main menu
                    CoronaWars.getInstance().setScreen(new MenuView());
                    // This also stops the game if a game is in progress
                    CoronaWars.getInstance().stopGame();
                }
                else if(screen.equals("MENU")){
                    CoronaWars.getInstance().setScreen(new MenuView());
                }
                else if(screen.equals("SETTINGS")){
                    CoronaWars.getInstance().setScreen(new SettingsView());
                }
                else if(screen.equals("TUTORIAL")){
                    CoronaWars.getInstance().setScreen(new TutorialView());
                }
                else if(screen.equals("GAMEOVER")){
                    CoronaWars.getInstance().setScreen(new GameOverView());
                }


            }
        });

    }


}
