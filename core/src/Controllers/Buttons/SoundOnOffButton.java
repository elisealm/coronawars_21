package Controllers.Buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import Controllers.SettingsController;

public class SoundOnOffButton {
    private boolean sound;
    private float x;
    private float y;
    private Button button;
    private Sprite buttonSprite;
    private SettingsController settings;

    public Button getButton() {
        return button;
    }

    public SoundOnOffButton(Sprite buttonSprite, float x, float y, final SettingsController settings) {
        this.buttonSprite = buttonSprite;
        this.x = x;
        this.y = y;
        this.button = new Button(new SpriteDrawable(buttonSprite));
        button.setPosition(x, y);
        this.sound = true;
        this.settings = settings;


        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent inputEvent, float xpos, float ypos) {
                settings.soundOnOff();
            }

        });
    }
}
