package Controllers;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.CoronaWars;

import java.util.Random;

import Models.Components.LifeTimeComponent;
import Models.Components.PositionComponent;
import Models.Components.SpriteComponent;
import Models.Components.VelocityComponent;

public class PowerUpSystem extends IteratingSystem {
    private static final Family family = Family.all(LifeTimeComponent.class).get();
    private ComponentMapper<LifeTimeComponent> lm = ComponentMapper.getFor(LifeTimeComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<SpriteComponent> sm = ComponentMapper.getFor(SpriteComponent.class);
    private Random random;


    public PowerUpSystem() {
        super(family);
        lm = ComponentMapper.getFor(LifeTimeComponent.class);
        vm = ComponentMapper.getFor(VelocityComponent.class);
        pm = ComponentMapper.getFor(PositionComponent.class);
        random = new Random();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        LifeTimeComponent lifeTimeComponent = lm.get(entity);
        VelocityComponent velocityComponent = vm.get(entity);
        PositionComponent positionComponent = pm.get(entity);
        SpriteComponent spriteComponent = sm.get(entity);
    }
    public void setSprite(Entity entity, Sprite sprite){
        sm.get(entity).sprite = sprite;
    }
    public Sprite getSprite(Entity entity){
        return sm.get(entity).sprite;
    }
    public void setLifeTime(Entity entity, int lifeTime){
        lm.get(entity).lifeTime = lifeTime;
    }
    public int getLifeTime(Entity entity){
        return lm.get(entity).lifeTime;
    }

    public void setShowTime(Entity entity){ lm.get(entity).showTime = 5 + random.nextInt(10);}
    public int getShowTime(Entity entity){return lm.get(entity).showTime;}

    public void setVelocity(Entity entity){
        vm.get(entity).x = random.nextInt(4) -2;
        vm.get(entity).y = random.nextInt(4) -2;
    }
    public int getVelocityx(Entity entity){ return vm.get(entity).x; }
    public int getVelocityy(Entity entity){ return vm.get(entity).y; }

    public void setPosition(Entity entity){
        pm.get(entity).pos_x = 350 + (random.nextInt(Gdx.graphics.getWidth() - 700));
        pm.get(entity).pos_y = 400 + (random.nextInt(Gdx.graphics.getHeight() - 800));
    }

    public int getPositionx(Entity entity){
       return pm.get(entity).pos_x;
    }
    public int getPositiony(Entity entity){ return pm.get(entity).pos_y;
    }

    public void updatePosition(Entity entity){
        pm.get(entity).pos_x += vm.get(entity).x;
        pm.get(entity).pos_y += vm.get(entity).y;
    }
    public void setShowing(Entity entity, boolean show){pm.get(entity).showing = show ;}
    public boolean getShowing(Entity entity){
       return pm.get(entity).showing;
    }

    public void showPowerUp(Entity entity, Batch batch, int i){
        int timeLeft = (int) CoronaWars.getInstance().getTimeLeft();

        if ((i == 1 )&& ((timeLeft - 15) < getShowTime(entity)) && (timeLeft - 15 + getLifeTime(entity) >= getShowTime(entity))){
            batch.draw(getSprite(entity), getPositionx(entity), getPositiony(entity));
            setShowing(entity,true);
        } else if ((i == 2) && ((timeLeft < getShowTime(entity)) && (timeLeft + getLifeTime(entity) >= getShowTime(entity)))){
            batch.draw(getSprite(entity), getPositionx(entity), getPositiony(entity));
            setShowing(entity,true);
        }else{
            setShowing(entity,false);
        }

    }
    public void showSuperPowerUp(Entity entity, Batch batch, int i){
        int timeLeft = (int) CoronaWars.getInstance().getTimeLeft();
        if ((i == 1) && ((timeLeft - 15) < getShowTime(entity)) && (timeLeft - 15 + getLifeTime(entity) >= getShowTime(entity))){
            batch.draw(getSprite(entity), getPositionx(entity), getPositiony(entity));
            updatePosition(entity);
            setShowing(entity,true);
        }else if ((i == 2) && ((timeLeft < getShowTime(entity)) && (timeLeft + getLifeTime(entity) >= getShowTime(entity)))){
            batch.draw(getSprite(entity), getPositionx(entity), getPositiony(entity));
            updatePosition(entity);
            setShowing(entity,true);
        }else{
            setShowing(entity,false);
        }
    }

}
