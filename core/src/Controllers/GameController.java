
package Controllers;

import com.badlogic.ashley.core.Engine;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mygdx.game.CoronaWars;

import Models.GameModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Models.OpponentModel;
import Models.PowerUp;
import Models.VirusModel;

import Models.state.PlayerOneState;
import Models.state.PlayerTwoState;
import Singleton.SoundSingleton;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;



public class GameController {
    public VirusModel virus;
    public List<VirusModel> viruses;
    public OpponentModel opponent;
    public GameModel gameModel;
    private SoundSingleton sound;
    private boolean IsMoving ;
    private int oldPosition;
    private int NumberMove;
    private boolean directionPositiv;
    private Sprite virusSprite;
    private TextureAtlas textureAtlas;
    private int score1;
    private int score2;

    //ASHLEY
    public PowerUpSystem powerUpSystem;
    public Engine engine;
    public Entity firstPowerUpEntity;
    public Entity secondPowerUpEntity;
    public Entity firstSuperPowerUpEntity;
    public Entity secondSuperPowerUpEntity;


    public GameController(){
        setupAshley();
        this.powerUpSystem = engine.getSystem(PowerUpSystem.class);

        this.opponent = new OpponentModel();
        this.viruses = new ArrayList<>();
        this.gameModel = new GameModel();
        this.sound = SoundSingleton.getInstance();

        this.IsMoving = false;
        this.textureAtlas = new TextureAtlas("covid_sprites.txt");
        this.virusSprite = textureAtlas.createSprite("covid_green");
        this.score1 = 10;
        this.score2= 10;


    }


    public void collisionCheck() {
        for (int v = 0; v < viruses.size(); v ++){
            if ((viruses.get(v).pos_x >= this.opponent.position_x ) && (viruses.get(v).pos_x <= this.opponent.position_x + opponent.sprite.getWidth())
            && (viruses.get(v).pos_y>= this.opponent.position_y) &&(viruses.get(v).pos_y <= this.opponent.position_y + opponent.sprite.getHeight())){
                if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
                    gameModel.setScore1(gameModel.getScore1() + this.score1);
                    if (sound.sound) {
                        if (this.score1 == 10) {
                            sound.sneeze1.play();
                        } else if (this.score1 == 20) {
                            sound.sneeze2.play();
                        }

                    }
                }else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerTwoState) {
                        gameModel.setScore2(gameModel.getScore2() + this.score2);
                        if (sound.sound) {
                            if (this.score2 == 10) {
                                sound.sneeze1.play();
                            } else if (this.score2 == 20) {
                                sound.sneeze2.play();
                            }
                        }
                    }
                viruses.remove(v);
            }
        }
    }

    public void powerUpsCollisionCheck(Entity entity, int type){
        powerUpSystem = engine.getSystem(PowerUpSystem.class);
        for (int v = 0; v < viruses.size(); v ++){
            if( powerUpSystem.getShowing(entity)) {
                if ((viruses.get(v).pos_x >= powerUpSystem.getPositionx(entity) && (viruses.get(v).pos_x <= powerUpSystem.getPositionx(entity) + powerUpSystem.getSprite(entity).getWidth()))
                        && (viruses.get(v).pos_y >= powerUpSystem.getPositiony(entity)) && (viruses.get(v).pos_y <= powerUpSystem.getPositiony(entity) + powerUpSystem.getSprite(entity).getHeight())) {
                    if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
                        //check what kind of powerUp
                        if (type == 1) {
                            gameModel.setScore1(gameModel.getScore1() + 20);
                        } else if (type == 2) {
                            this.virusSprite = textureAtlas.createSprite("mutant");
                            gameModel.setScore1(gameModel.getScore1() + 20);
                            this.score1 = 20;
                        }
                    } else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerTwoState) {
                        if (type == 1) {
                            gameModel.setScore2(gameModel.getScore2() + 20);

                        } else if (type == 2) {
                            this.virusSprite = textureAtlas.createSprite("mutant");
                            gameModel.setScore2(gameModel.getScore2() + 20);
                            this.score2 = 20;
                        }
                    }
                    viruses.remove(v);
                    if (sound.sound){
                        sound.superPowerUpSound.play();
                    }
                }
            }
        }
    }

    public Sprite getVirusSprite(){ return virusSprite;}

    public void setScorePlayerOne(){
        CoronaWars.getInstance().getGameStateController().setScore1(gameModel.getScore1());
    }

    public void setScorePlayerTwo(){
        CoronaWars.getInstance().getGameStateController().setScore2(gameModel.getScore2());
    }


    public int getScore(){
        if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
            return gameModel.getScore1();
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerTwoState) {
            return gameModel.getScore2();
        }
        else {
            return 0;
        }

    }

    public Sprite getPlayer(){
        if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
            return gameModel.getPlayerOneText();
        }
        else {
            return gameModel.getPlayerTwoText();
        }

    }

    public void virus(int xpos, int ypos) {
        this.virus = new VirusModel(xpos, ypos);
        viruses.add(virus);

    }

    public void moveForward() {
        for (int v =0; v< viruses.size(); v++)
            viruses.get(v).pos_y += 20;
        //virus.pos_y += 20;
    }

    public void moveOpponent(){
        if (IsMoving == false) {
            Random random = new Random();
            NumberMove = random.nextInt(200) + 50;
            oldPosition = opponent.position_x;
            if (NumberMove % 2 == 0) {
                NumberMove = -NumberMove;
                directionPositiv = false;
            }else{
                directionPositiv = true;
            }

            if ((opponent.position_x + opponent.sprite.getWidth() + NumberMove< Gdx.graphics.getWidth() - 300) && opponent.position_x + NumberMove > 300) {
                this.IsMoving = true;
                while ( opponent.position_x !=  oldPosition + NumberMove) {
                    if (directionPositiv ) {
                        opponent.position_x += 1;

                    }else {
                        opponent.position_x -= 1;
                    }
                }
            }
            if (opponent.position_x ==  oldPosition + NumberMove){
                this.IsMoving = false;
            }
        }



    }
    //Setup Ashley
    private void setupAshley(){
        this.engine = new Engine();
        PowerUp ashleyPowerUp = new PowerUp(engine);

        //ADD system to engine
        engine.addSystem(new PowerUpSystem());

        //create powerups
        firstPowerUpEntity = ashleyPowerUp.createPowerUp();
        secondPowerUpEntity = ashleyPowerUp.createPowerUp();
        firstSuperPowerUpEntity = ashleyPowerUp.createSuperPowerUp();
        secondSuperPowerUpEntity = ashleyPowerUp.createSuperPowerUp();

        powerUpSystem = engine.getSystem(PowerUpSystem.class);
    }

}
