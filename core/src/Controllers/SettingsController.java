package Controllers;

import Singleton.BackgroundColor;
import Singleton.SoundSingleton;

public class SettingsController {
    private BackgroundColor backgroundColor;
    private SoundSingleton sound;

    public SettingsController() {
        this.backgroundColor = BackgroundColor.getInstance();
        this.sound = SoundSingleton.getInstance();

    }

    public void soundOnOff(){
        if (sound.sound){
            sound.soundFile.pause();
            sound.sound = false;
            System.out.println("hei");
        }else {
            sound.soundFile.play();
            sound.sound = true;
            System.out.println("hei");
        }
    }

    public void changeBackground(String color) {
        if (color.equals("brown")) {
            System.out.println("brown pressed");
            backgroundColor.redNumber = 170;
            backgroundColor.greenNumber = 163;
            backgroundColor.blueNumber = 164;
            System.out.println(backgroundColor.redNumber);
        } else if (color.equals("purple")) {
            System.out.println("purple pressed");
            backgroundColor.redNumber = 176;
            backgroundColor.greenNumber = 177;
            backgroundColor.blueNumber = 212;
            System.out.println(backgroundColor.redNumber);
        } else if (color.equals("green")) {
            System.out.println("green pressed");
            backgroundColor.redNumber = 184;
            backgroundColor.greenNumber = 200;
            backgroundColor.blueNumber = 199;
            System.out.println(backgroundColor.redNumber);
        }

    }
}
