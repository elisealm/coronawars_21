package Controllers;

import com.mygdx.game.CoronaWars;

import Models.GameStateModel;
import Models.state.GameOverState;
import Models.state.PlayerOneState;
import Models.state.PlayerTwoState;
import Models.state.PrePlayerOneState;
import Models.state.PrePlayerTwoState;

public class GameStateController {

    private boolean gameStarted;
    private boolean startTimeSet;
    private long startTime;
    private long elapsedTime;
    private long timeLeft;

    private int score1;
    private int score2;

    private GameStateModel gameStateModel;


    public GameStateController(boolean gameStarted) {

        this.gameStarted = gameStarted;
        this.gameStateModel = new GameStateModel(this.gameStarted, this.elapsedTime, this.timeLeft);

    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public void setStartTimeSet(boolean startTimeSet) {
        this.startTimeSet = startTimeSet;
    }

    public GameStateModel getGameStateModel() {
        return gameStateModel;
    }

    public long getTimeLeft() {
        return gameStateModel.getTimeLeft();
    }

    public void checkGameStateChanged() {

        if(CoronaWars.getInstance().gsm.currentState() instanceof PrePlayerOneState) {
            if (gameStateModel.isGameStarted()) {
                if (!startTimeSet) {
                    startTime = System.currentTimeMillis();
                    startTimeSet = true;
                }
                // Stay in player two state for 3 seconds
                long currentTime = System.currentTimeMillis();
                elapsedTime = currentTime - startTime;
                gameStateModel.setElapsedTime(currentTime-startTime);
                gameStateModel.setTimeLeft(3 - gameStateModel.getElapsedTime()/1000);
                if ((gameStateModel.getElapsedTime()) / 1000 >= 3) {

                    // Change state and view
                    CoronaWars.getInstance().gsm.currentState().changeState(new PlayerOneState(CoronaWars.getInstance().gsm));
                    startTimeSet = false;
                }
                System.out.println((elapsedTime) / 1000);
            }
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
            if (gameStateModel.isGameStarted()) {
                if (!startTimeSet) {
                    startTime = System.currentTimeMillis();
                    startTimeSet = true;
                }
                // Stay in player two state for 30 seconds
                long currentTime = System.currentTimeMillis();
                elapsedTime = currentTime - startTime;
                gameStateModel.setElapsedTime(currentTime-startTime);
                gameStateModel.setTimeLeft(30 - gameStateModel.getElapsedTime()/1000);
                if ((gameStateModel.getElapsedTime()) / 1000 >= 30) {

                    // Change state and view
                    CoronaWars.getInstance().gsm.currentState().changeState(new PrePlayerTwoState(CoronaWars.getInstance().gsm));
                    startTimeSet = false;
                }
                System.out.println((elapsedTime) / 1000);
            }
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PrePlayerTwoState) {
            if (gameStateModel.isGameStarted()) {
                if (!startTimeSet) {
                    startTime = System.currentTimeMillis();
                    startTimeSet = true;
                }
                // Stay in pre player two state for 3 seconds
                long currentTime = System.currentTimeMillis();
                elapsedTime = currentTime - startTime;
                gameStateModel.setElapsedTime(currentTime-startTime);
                gameStateModel.setTimeLeft(3 - gameStateModel.getElapsedTime()/1000);
                if ((gameStateModel.getElapsedTime()) / 1000 >= 3) {

                    // Change state and view
                    CoronaWars.getInstance().gsm.currentState().changeState(new PlayerTwoState(CoronaWars.getInstance().gsm));
                    startTimeSet = false;
                }
                System.out.println((elapsedTime) / 1000);
            }
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerTwoState) {
            if (gameStateModel.isGameStarted()) {
                if (!startTimeSet) {
                    startTime = System.currentTimeMillis();
                    startTimeSet = true;
                }
                long currentTime = System.currentTimeMillis();
                gameStateModel.setElapsedTime(currentTime-startTime);
                gameStateModel.setTimeLeft(30 - gameStateModel.getElapsedTime()/1000);
                // Stay in player two state for 30 seconds
                if ((gameStateModel.getElapsedTime()) / 1000 >= 30) {
                    // Change state and view
                    CoronaWars.getInstance().gsm.currentState().changeState(new GameOverState(CoronaWars.getInstance().gsm));
                    startTimeSet = false;
                }
                System.out.println((currentTime - startTime) / 1000);
            }
        } else if (CoronaWars.getInstance().gsm.currentState() instanceof GameOverState) {
            // When we get to gameOverState, timer should stop and state should be changed to playerOneState (ready for new game)
            if (gameStateModel.isGameStarted()) {
                CoronaWars.getInstance().gsm.currentState().changeState(new PrePlayerOneState(CoronaWars.getInstance().gsm));
                startTimeSet = false;
                gameStateModel.setGameStarted(false);
                }
            }
        }
    }
