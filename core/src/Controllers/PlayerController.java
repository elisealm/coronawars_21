package Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;


import Models.PlayerModel;

public class PlayerController {

    private final PlayerModel player;
    public boolean leftDown;
    public boolean rightDown;
    public PlayerController(int posx, int posy){
        this.player = new PlayerModel(posx, posy, 1000);
        this.leftDown = false;
        this.rightDown = false;
    }

    public void moveRight() {
        if (rightDown) {
            if (player.position_x+player.sprite.getWidth() < Gdx.graphics.getWidth()-300) {
                player.position_x += 10;
            }
        }
    }
    public void moveLeft() {
        if (leftDown) {
            if (player.position_x > 300) {
                player.position_x -= 10;
            }
        }
    }

    public PlayerModel getPlayer() {
        moveRight();
        moveLeft();
        return this.player;
    }


}

