package Singleton;

public class BackgroundColor{

    private static BackgroundColor instance = new BackgroundColor();

    public int redNumber;
    public int greenNumber;
    public int blueNumber;

        private BackgroundColor() {
            this.redNumber = 170;
            this.greenNumber = 163;
            this.blueNumber = 164;
        }

        public static BackgroundColor getInstance() {
            return instance;
        }

    }

