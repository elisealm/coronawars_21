package Singleton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;


import Controllers.SettingsController;

public class SoundSingleton extends SettingsController {

    public boolean sound;
    public Music soundFile;
    public Sound sneeze1;
    public Sound sneeze2;
    public Sound superPowerUpSound;


    private static final SoundSingleton INSTANCE = new SoundSingleton();

    private SoundSingleton() {
        this.sound = true;
        this.soundFile = Gdx.audio.newMusic(Gdx.files.internal("Music/music.mp3"));
        this.soundFile.play();
        this.soundFile.setVolume(0.8f);
        this.soundFile.setLooping(true);
        this.sneeze1 = Gdx.audio.newSound(Gdx.files.internal("Music/sneeze.mp3"));
        this.sneeze2 = Gdx.audio.newSound(Gdx.files.internal("Music/sneeze2.wav"));
        this.superPowerUpSound = Gdx.audio.newSound(Gdx.files.internal("Music/superPowerUp.wav"));
    }

    public static SoundSingleton getInstance() {
            return INSTANCE;
        }

}
