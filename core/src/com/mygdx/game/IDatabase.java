package com.mygdx.game;

import Models.HighscoreHolder;

public interface IDatabase {

    void SetHighscoreListener(HighscoreHolder highscoreHolder);


    void SetNewHighscore(String name, int score);

}
