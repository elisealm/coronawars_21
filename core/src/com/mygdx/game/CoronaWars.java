package com.mygdx.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Controllers.GameController;
import Controllers.GameStateController;
import Models.HighscoreHolder;
import Models.state.GameOverState;
import Models.state.GameStateManager;
import Models.state.PlayerOneState;
import Models.state.PlayerTwoState;
import Models.state.PrePlayerOneState;
import Views.GameOverView;
import Views.GameView;
import Views.HighScoreView;
import Views.MenuView;


public class CoronaWars extends Game {

	public SpriteBatch batch;
	public IDatabase _FBC;
	public GameStateManager gsm;
	private Models.HighscoreHolder highscoreHolder;
	private GameStateController gameStateController;
	private boolean gameStarted;

	private static CoronaWars INSTANCE;

	public static CoronaWars getInstance(IDatabase FBC) {
		if (INSTANCE == null) {
			INSTANCE = new CoronaWars(FBC);
		}
		return INSTANCE;
	}

	public static CoronaWars getInstance() {
		return INSTANCE;
	}


	private CoronaWars(IDatabase FBC){
		_FBC = FBC;
	}


	public HighscoreHolder getHighscoreHolder() {
		return highscoreHolder;
	}

	public GameStateController getGameStateController() {
		return gameStateController;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		highscoreHolder = new HighscoreHolder();
		_FBC.SetHighscoreListener(highscoreHolder);

		// Initialize gamestarted - change to true when clicking Play Game button (In ChangeViewButton)
		gameStarted = false;

		// Set GameStateManager
		gsm = new GameStateManager();
		// Initialize to PlayerOneState
		gsm.push(new PrePlayerOneState(gsm));

		// Set initial screen
		setScreen(new MenuView());

		// Initialize gameStateController
		gameStateController = new GameStateController(gameStarted);

	}


	@Override
	public void render () {

		// Manage states if game has started through gameStateController
		gameStateController.checkGameStateChanged();

		super.render();
	}

	public void startGame() {
		gameStateController.getGameStateModel().setGameStarted(true);
	}

	// Stop the game and reset variables related to active game.
	public void stopGame() {
		gameStateController.getGameStateModel().setGameStarted(false);
		gameStateController.setStartTimeSet(false);
		CoronaWars.getInstance().gsm.currentState().reset();
	}

	public long getTimeLeft() {
		return gameStateController.getTimeLeft();
	}
	
	@Override
	public void dispose () {
		batch.dispose();

	}

}
