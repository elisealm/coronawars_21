package Views;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.CoronaWars;


import java.util.Map;

import Controllers.Buttons.ChangeViewButton;
import Models.HighscoreHolder;
import Singleton.BackgroundColor;


public class HighScoreView implements Screen {
    private ChangeViewButton mainmenuScreenButton;
    private TextureAtlas textureAtlas;
    private TextureAtlas textureAtlasTitle;
    private Sprite mainMenu;
    private Sprite title;
    private Stage stage;
    private Skin skin;

    private Table highscoreResults;
    private Table scrollHighscoreResults;
    HighscoreHolder highscoreHolder;
    private BackgroundColor backgroundColor;


    public HighScoreView(){
        this.stage = new Stage();
        this.textureAtlas = new TextureAtlas("logo_buttons.txt");
        this.textureAtlasTitle = new TextureAtlas("settings_highscore_sprites.txt");
        this.mainMenu = textureAtlas.createSprite("mainmenu2x");
        this.mainmenuScreenButton = new ChangeViewButton(mainMenu, "MENU", Gdx.graphics.getWidth()/2-mainMenu.getWidth()/2, 100);
        this.stage.addActor(mainmenuScreenButton.getButton());
        this.backgroundColor = BackgroundColor.getInstance();
        this.skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));

        this.title = textureAtlasTitle.createSprite("highscores");

        Gdx.input.setInputProcessor(stage);

    }
    @Override
    public void show() {

        makeTable();
        stage.addActor(scrollHighscoreResults);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        CoronaWars.getInstance().batch.begin();
        CoronaWars.getInstance().batch.draw(title, Gdx.graphics.getWidth()/2-title.getWidth()/2, Gdx.graphics.getHeight()-title.getHeight()-100);
        CoronaWars.getInstance().batch.end();

        stage.act();
        stage.draw();
    }

    private void makeTable() {

        Label.LabelStyle style= new Label.LabelStyle();
        style.font = new BitmapFont();
        style.fontColor = Color.BLACK;
        style.font.getData().setScale(4);
        highscoreHolder = CoronaWars.getInstance().getHighscoreHolder();
        Map<String, Long> highscores = highscoreHolder.getScoresDescending();

        highscoreResults =  new Table();
        highscoreResults.setPosition(Gdx.graphics.getWidth()/2f + highscoreResults.getWidth(), Gdx.graphics.getHeight()/2f);
        highscoreResults.center();

        Label rank;
        Label name;
        Label score;

        rank = new Label("Rank", style);
        name = new Label("Name", style);
        score = new Label("Score", style);

        highscoreResults.add(rank);
        highscoreResults.add(name);
        highscoreResults.add(score);
        highscoreResults.getCell(rank).height(90).width(300);
        highscoreResults.getCell(name).height(90).width(400);
        highscoreResults.getCell(score).height(90).width(300);
        highscoreResults.row();

        int i = 1;
        for (String key : highscores.keySet()) {

            rank = new Label(i + ". ", style);
            name = new Label(key, style);
            score = new Label(highscores.get(key).toString(), style);
            i++;

            highscoreResults.add(rank);
            highscoreResults.add(name);
            highscoreResults.add(score);
            highscoreResults.getCell(rank).height(90).width(300);
            highscoreResults.getCell(name).height(90).width(400);
            highscoreResults.getCell(score).height(90).width(300);
            highscoreResults.row();
        }

        final ScrollPane scroller = new ScrollPane(highscoreResults, skin);
        scroller.setFadeScrollBars(false);
        scrollHighscoreResults = new Table();
        scrollHighscoreResults.setFillParent(true);
        scrollHighscoreResults.add(scroller).width(Gdx.graphics.getWidth()/2f + highscoreResults.getWidth()).height(Gdx.graphics.getHeight()/2f);
        scrollHighscoreResults.row();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
        textureAtlasTitle.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
