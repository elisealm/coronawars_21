package Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.CoronaWars;

import Controllers.Buttons.ChangeViewButton;
import Singleton.BackgroundColor;
import Singleton.SoundSingleton;

public class MenuView implements Screen {

    private ChangeViewButton gameScreenButton;
    private ChangeViewButton highscoreScreenButton;
    private ChangeViewButton settingsScreenButton;
    private ChangeViewButton tutorialScreenButton;
    private TextureAtlas textureAtlas;
    private TextureAtlas textureAtlasMisc;
    private Sprite playGame;
    private Sprite highscore;
    private Sprite settings;
    private Sprite tutorial;
    private Sprite logo;
    private Stage stage;
    private BackgroundColor backgroundColor;
    private SoundSingleton sound;


    public MenuView(){
        this.stage = new Stage();
        this.backgroundColor = BackgroundColor.getInstance();
        this.sound = SoundSingleton.getInstance();

        this.textureAtlas = new TextureAtlas("logo_buttons.txt");
        this.textureAtlasMisc = new TextureAtlas("misc_sprites.txt");
        this.playGame = textureAtlas.createSprite("playgame2x");
        this.highscore = textureAtlas.createSprite("highscores2x");
        this.settings = textureAtlas.createSprite("settings2x");
        this.tutorial = textureAtlasMisc.createSprite("tutorialsmall2x");
        this.logo = textureAtlas.createSprite("logo4x");

        this.highscoreScreenButton = new ChangeViewButton(highscore, "HIGHSCORE", Gdx.graphics.getWidth()/2-playGame.getWidth()-highscore.getWidth()/2, 300);
        this.gameScreenButton = new ChangeViewButton(playGame, "GAME", Gdx.graphics.getWidth()/2-playGame.getWidth()/2, 300);
        this.settingsScreenButton = new ChangeViewButton(settings, "SETTINGS", Gdx.graphics.getWidth()/2 + playGame.getWidth()/2, 300);
        this.tutorialScreenButton = new ChangeViewButton(tutorial, "TUTORIAL", Gdx.graphics.getWidth()/2-tutorial.getWidth()/2, 150);

        stage.addActor(highscoreScreenButton.getButton());
        stage.addActor(gameScreenButton.getButton());
        stage.addActor(settingsScreenButton.getButton());
        stage.addActor(tutorialScreenButton.getButton());

        Gdx.input.setInputProcessor(stage);


    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        CoronaWars.getInstance().batch.begin();
        CoronaWars.getInstance().batch.draw(logo, Gdx.graphics.getWidth()/2 - logo.getWidth()/2, 600);
        CoronaWars.getInstance().batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
        textureAtlasMisc.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
