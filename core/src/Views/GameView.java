package Views;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.CoronaWars;

import Controllers.Buttons.ShootButton;
import Controllers.GameController;
import Controllers.PowerUpSystem;
import Controllers.Buttons.ChangeViewButton;
import Controllers.Buttons.MovementButton;
import Controllers.PlayerController;
import Models.PowerUp;
import Models.state.PlayerOneState;
import Models.state.PlayerTwoState;
import Singleton.BackgroundColor;


public class GameView implements Screen {
    private final Stage stage;

    //textureAtlas
    private TextureAtlas textureAtlas;
    private TextureAtlas textureAtlasText;
    private TextureAtlas textureAtlasMisc;

    //buttons
    private MovementButton moveRightButton;
    private MovementButton moveLeftButton;
    private ShootButton shootButtonRight;
    private ShootButton shootButtonLeft;
    private ChangeViewButton exitButton;

    //sprites
    private Sprite moveRightButton_sprite;
    private Sprite moveLeftButton_sprite;
    private Sprite green_virus;
    private Sprite shootButtonSprite;
    private Sprite exit;
    private Sprite scoreText;
    private Sprite timeLeftText;

    //singleton
    private final BackgroundColor backgroundColor;

    //controllers
    private PlayerController player;
    private GameController gameController;

    //fonts
    private BitmapFont displayScore;
    private BitmapFont displayTimerText;

    private long timeLeft;


    public GameView(){
        super();

        this.stage = new Stage(new ScreenViewport());

        //gameController
        this.gameController = new GameController();
        this.player = new PlayerController(Gdx.graphics.getWidth()/2, 0);
        this.gameController.virus(player.getPlayer().getPosition_x()+200, player.getPlayer().getPosition_y()+140);

        //singleton
        this.backgroundColor = BackgroundColor.getInstance();

        //textureAtlas
        this.textureAtlasText = new TextureAtlas("save_score_sprites.txt");
        this.textureAtlas = new TextureAtlas("arrow_sprites.txt");
        this.textureAtlasMisc = new TextureAtlas("misc_sprites.txt");

        //sprites
        this.green_virus = gameController.getVirusSprite();
        this.shootButtonSprite = textureAtlasMisc.createSprite("shootbutton2x");
        this.moveRightButton_sprite = textureAtlas.createSprite("right2x");
        this.moveLeftButton_sprite = textureAtlas.createSprite("left2x");
        this.moveRightButton_sprite = textureAtlas.createSprite("right2x");
        this.moveLeftButton_sprite = textureAtlas.createSprite("left2x");

        // Text sprites
        this.timeLeftText = textureAtlasText.createSprite("timeleft");
        this.scoreText = textureAtlasText.createSprite("score");

        //buttons
        this.exit = textureAtlasMisc.createSprite("quitgamesmall2x");
        this.exitButton = new ChangeViewButton(exit, "QUIT", Gdx.graphics.getWidth()-exit.getWidth(), Gdx.graphics.getHeight()-(exit.getHeight()));
        this.moveRightButton = new MovementButton(moveRightButton_sprite, Gdx.graphics.getWidth()-moveRightButton_sprite.getWidth()-70, 30, player, "right");
        this.moveLeftButton = new MovementButton(moveLeftButton_sprite, moveRightButton_sprite.getWidth()-130, 30, player, "left");
        this.shootButtonRight = new ShootButton(shootButtonSprite, Gdx.graphics.getWidth()-shootButtonSprite.getWidth()-87, 240, player, gameController, "right");
        this.shootButtonLeft = new ShootButton(shootButtonSprite, shootButtonSprite.getWidth()-87, 240, player, gameController, "left");

        //actors
        stage.addActor(exitButton.getButton());
        stage.addActor(moveRightButton.getButton());
        stage.addActor(moveLeftButton.getButton());
        stage.addActor(shootButtonLeft.getButton());
        stage.addActor(shootButtonRight.getButton());

        //fonts
        this.displayScore = new BitmapFont();
        this.displayTimerText = new BitmapFont();

    }



    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Get timeLeft from CoronaWars' gameStateController()
        this.timeLeft = CoronaWars.getInstance().getTimeLeft();

        this.green_virus = gameController.getVirusSprite();

        CoronaWars.getInstance().batch.begin();

        //draw powerUps
        gameController.powerUpSystem.showPowerUp(gameController.firstPowerUpEntity, CoronaWars.getInstance().batch, 1);
        gameController.powerUpSystem.showPowerUp(gameController.secondPowerUpEntity, CoronaWars.getInstance().batch, 2);
        gameController.powerUpSystem.showSuperPowerUp(gameController.firstSuperPowerUpEntity, CoronaWars.getInstance().batch, 1);
        gameController.powerUpSystem.showSuperPowerUp(gameController.secondSuperPowerUpEntity, CoronaWars.getInstance().batch,2);

        //check collision with powerUps
        gameController.powerUpsCollisionCheck(gameController.firstPowerUpEntity,  1);
        gameController.powerUpsCollisionCheck(gameController.secondPowerUpEntity,  1);
        gameController.powerUpsCollisionCheck(gameController.firstSuperPowerUpEntity,  2);
        gameController.powerUpsCollisionCheck(gameController.secondSuperPowerUpEntity,  2);

        //Display player text
        CoronaWars.getInstance().batch.draw(gameController.getPlayer(), 50, (Gdx.graphics.getHeight()-100));
        CoronaWars.getInstance().batch.draw(scoreText, ((100 + gameController.getPlayer().getWidth())), (Gdx.graphics.getHeight()-100));

        //Display score of currently playing player
        this.displayScore.setColor(Color.BLACK);
        this.displayScore.getData().setScale(3);
        this.displayScore.draw(CoronaWars.getInstance().batch, String.valueOf(gameController.getScore()), (110 + gameController.getPlayer().getWidth()+ scoreText.getWidth()),(Gdx.graphics.getHeight()-50));

        //display time left of currently playing player
        CoronaWars.getInstance().batch.draw(timeLeftText,(200 + gameController.getPlayer().getWidth()+ scoreText.getWidth()),(Gdx.graphics.getHeight()-100));
        this.displayTimerText.setColor(Color.BLACK);
        this.displayTimerText.getData().setScale(3);
        this.displayTimerText.draw(CoronaWars.getInstance().batch, String.valueOf(timeLeft)+"s",(210 + gameController.getPlayer().getWidth()+ scoreText.getWidth()) + timeLeftText.getWidth(),(Gdx.graphics.getHeight()-50));

        //draw player and opponent
        CoronaWars.getInstance().batch.draw(player.getPlayer().sprite, player.getPlayer().getPosition_x(), player.getPlayer().getPosition_y());
        CoronaWars.getInstance().batch.draw(gameController.opponent.sprite, gameController.opponent.position_x, gameController.opponent.position_y);


        if (System.currentTimeMillis()%10 == 0){
            gameController.moveOpponent();
        }

        //draw virus on button click
        if (gameController.gameModel.isShootButtonBeenClicked()) {
            for (int v = 1 ; v < gameController.viruses.size(); v++){
                CoronaWars.getInstance().batch.draw(green_virus ,gameController.viruses.get(v).pos_x, gameController.viruses.get(v).pos_y);
            }
            gameController.moveForward();
        }
        gameController.collisionCheck();

        // save score each render
        if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerOneState) {
            gameController.setScorePlayerOne();
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PlayerTwoState) {
            gameController.setScorePlayerTwo();
        }

        CoronaWars.getInstance().batch.end();
        stage.act();
        stage.draw();

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
        textureAtlasText.dispose();
        textureAtlasMisc.dispose();
        stage.dispose();

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}

