package Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mygdx.game.CoronaWars;

import Models.state.PrePlayerOneState;
import Models.state.PrePlayerTwoState;
import Singleton.BackgroundColor;

public class PreGameView implements Screen {

    private TextureAtlas textureAtlas;
    private Sprite playerOneGo;
    private Sprite playerTwoGo;
    private final BackgroundColor backgroundColor;
    private BitmapFont displayCountdown;

    public PreGameView() {
        super();
        this.textureAtlas = new TextureAtlas("misc_sprites.txt");
        this.playerOneGo = textureAtlas.createSprite("player1go2x");
        this.playerTwoGo = textureAtlas.createSprite("player2go2x");
        this.backgroundColor = BackgroundColor.getInstance();
        this.displayCountdown = new BitmapFont();

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        CoronaWars.getInstance().batch.begin();

        if (CoronaWars.getInstance().gsm.currentState() instanceof PrePlayerOneState) {
            CoronaWars.getInstance().batch.draw(playerOneGo, Gdx.graphics.getWidth()/2-playerOneGo.getWidth()/2, Gdx.graphics.getHeight()/2-playerOneGo.getHeight()/2+100);
        }
        else if (CoronaWars.getInstance().gsm.currentState() instanceof PrePlayerTwoState) {
            CoronaWars.getInstance().batch.draw(playerTwoGo, Gdx.graphics.getWidth()/2-playerTwoGo.getWidth()/2, Gdx.graphics.getHeight()/2-playerTwoGo.getHeight()/2+100);
        }

        //Display score of currently playing player
        this.displayCountdown.setColor(Color.BLACK);
        this.displayCountdown.getData().setScale(7);
        this.displayCountdown.draw(CoronaWars.getInstance().batch, String.valueOf(CoronaWars.getInstance().getGameStateController().getGameStateModel().getTimeLeft()), Gdx.graphics.getWidth()/2-playerTwoGo.getWidth()/2+200, Gdx.graphics.getHeight()/2-playerTwoGo.getHeight()/2-100);

        CoronaWars.getInstance().batch.end();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
    }
}
