package Views;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.mygdx.game.CoronaWars;

import Controllers.Buttons.ChangeViewButton;
import Controllers.Buttons.SubmitHighscoreButton;
import Singleton.BackgroundColor;


public class GameOverView implements Screen {
    ChangeViewButton mainmenuButton;
    ChangeViewButton highscoresButton;
    SubmitHighscoreButton addHighscoreButtonPlayer1;
    SubmitHighscoreButton addHighscoreButtonPlayer2;
    TextureAtlas textureAtlasText;
    TextureAtlas textureAtlasButtons;
    TextureAtlas textureAtlasDraw;
    Sprite mainMenu;
    Sprite highscores;
    Sprite addHighscore;
    Sprite winner;
    Sprite playerOneName;
    Sprite playerOneScoreText;
    Sprite playerTwoName;
    Sprite playerTwoScoreText;
    Sprite highscoreSavedText;
    int p1score;
    int p2score;
    String inputPlayer1;
    String inputPlayer2;
    private BitmapFont displayScoreOne;
    private BitmapFont displayScoreTwo;
    Stage stage;
    private boolean Submit1IsPressed;
    private boolean Submit2IsPressed;
    
    private Skin skin;

    private BackgroundColor backgroundColor;
    final TextField usernamePlayerOne;
    final TextField usernamePlayerTwo;


    public GameOverView(){
        this.stage = new Stage();
        this.backgroundColor = BackgroundColor.getInstance();
        this.Submit1IsPressed= false;
        this.Submit2IsPressed = false;

        // Texture atlas
        this.textureAtlasButtons = new TextureAtlas("logo_buttons.txt");
        this.textureAtlasText = new TextureAtlas("save_score_sprites.txt");
        this.textureAtlasDraw = new TextureAtlas("settings_highscore_sprites.txt");

        // Buttons
        this.mainMenu = textureAtlasButtons.createSprite("mainmenu2x");
        this.highscores = textureAtlasButtons.createSprite("highscores2x");
        this.addHighscore = textureAtlasText.createSprite("addhighscore");
        this.mainmenuButton = new ChangeViewButton(mainMenu, "MENU", Gdx.graphics.getWidth()/3-mainMenu.getWidth()/2, Gdx.graphics.getHeight()-mainMenu.getHeight()-850);
        this.highscoresButton = new ChangeViewButton(highscores, "HIGHSCORE", Gdx.graphics.getWidth()*2/3-highscores.getWidth()/2, Gdx.graphics.getHeight()-highscores.getHeight()-850);
        this.addHighscoreButtonPlayer1 = new SubmitHighscoreButton(addHighscore, Gdx.graphics.getWidth()/4-addHighscore.getWidth()/2, Gdx.graphics.getHeight()-addHighscore.getHeight()-500);
        this.addHighscoreButtonPlayer2 = new SubmitHighscoreButton(addHighscore, Gdx.graphics.getWidth()*3/4-addHighscore.getWidth()/2, Gdx.graphics.getHeight()-addHighscore.getHeight()-500);

        stage.addActor(mainmenuButton.getButton());
        stage.addActor(highscoresButton.getButton());
        stage.addActor(addHighscoreButtonPlayer1.getButton());
        stage.addActor(addHighscoreButtonPlayer2.getButton());

        // Textfields
        this.playerOneName = textureAtlasText.createSprite("player1name");
        this.playerOneScoreText = textureAtlasText.createSprite("player1score");
        this.playerTwoName = textureAtlasText.createSprite("player2name");
        this.playerTwoScoreText = textureAtlasText.createSprite("player2score");
        this.highscoreSavedText = textureAtlasText.createSprite("highscore_saved");

        // score
        this.p1score = CoronaWars.getInstance().getGameStateController().getScore1();
        this.p2score = CoronaWars.getInstance().getGameStateController().getScore2();
        this.displayScoreOne = new BitmapFont();
        this.displayScoreTwo = new BitmapFont();

        // Title depending on which player won
        if(p1score > p2score) {
            winner = textureAtlasText.createSprite("player1won");
        }
        else if(p2score > p1score) {
            winner = textureAtlasText.createSprite("player2won");
        }
        else{
            winner = textureAtlasDraw.createSprite("draw");
        }


        // Add new name + skin
        this.skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
        skin.getFont("font").getData().setScale(2.5f);

        this.usernamePlayerOne = new TextField("", skin);
        this.usernamePlayerTwo = new TextField("", skin);

        stage.addActor(usernamePlayerOne);
        stage.addActor(usernamePlayerTwo);

        usernamePlayerOne.setMaxLength(10);
        usernamePlayerOne.setPosition(Gdx.graphics.getWidth()/4+playerOneScoreText.getWidth()/4, Gdx.graphics.getHeight()-playerOneName.getHeight()-400);
        usernamePlayerOne.setWidth(350f);
        this.inputPlayer1 = usernamePlayerOne.getText();

        usernamePlayerTwo.setMaxLength(10);
        usernamePlayerTwo.setPosition(Gdx.graphics.getWidth()*3/4+playerTwoScoreText.getWidth()/4, Gdx.graphics.getHeight()-playerTwoName.getHeight()-400);
        usernamePlayerTwo.setWidth(350f);
        this.inputPlayer2 = usernamePlayerTwo.getText();

        stage.addActor(usernamePlayerOne);
        stage.addActor(usernamePlayerTwo);

        Gdx.input.setInputProcessor(stage);

    }
    @Override
    public void show() {


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        this.displayScoreOne.setColor(Color.BLACK);
        this.displayScoreOne.getData().setScale(3);

        this.displayScoreTwo.setColor(Color.BLACK);
        this.displayScoreTwo.getData().setScale(3);

        CoronaWars.getInstance().batch.begin();
        CoronaWars.getInstance().batch.draw(winner, Gdx.graphics.getWidth()/2-winner.getWidth()/2, Gdx.graphics.getHeight()-winner.getHeight()-100);
        CoronaWars.getInstance().batch.draw(playerOneScoreText, Gdx.graphics.getWidth()/4-playerOneScoreText.getWidth(), Gdx.graphics.getHeight()-playerOneScoreText.getHeight()-300);
        CoronaWars.getInstance().batch.draw(playerOneName, Gdx.graphics.getWidth()/4-playerOneName.getWidth(), Gdx.graphics.getHeight()-playerOneName.getHeight()-400);
        CoronaWars.getInstance().batch.draw(playerTwoScoreText, Gdx.graphics.getWidth()*3/4-playerTwoScoreText.getWidth(), Gdx.graphics.getHeight()-playerTwoScoreText.getHeight()-300);
        CoronaWars.getInstance().batch.draw(playerTwoName, Gdx.graphics.getWidth()*3/4-playerTwoName.getWidth(), Gdx.graphics.getHeight()-playerTwoName.getHeight()-400);
        displayScoreOne.draw(CoronaWars.getInstance().batch, String.valueOf(p1score), Gdx.graphics.getWidth()/4+playerOneScoreText.getWidth()/4, Gdx.graphics.getHeight()-310);
        displayScoreTwo.draw(CoronaWars.getInstance().batch, String.valueOf(p2score), Gdx.graphics.getWidth()*3/4+playerTwoScoreText.getWidth()/4, Gdx.graphics.getHeight()-310);


        // Add highscore player 1
        if(addHighscoreButtonPlayer1.isSubmit()) {
            if ( this.Submit1IsPressed ==false ){
                submitScore(usernamePlayerOne.getText(), p1score);
                this.Submit1IsPressed = true;
            }
            CoronaWars.getInstance().batch.draw(highscoreSavedText, Gdx.graphics.getWidth()/4-highscoreSavedText.getWidth(), Gdx.graphics.getHeight()-highscoreSavedText.getHeight()-450);
        }

        // Add highscore player 2
        if(addHighscoreButtonPlayer2.isSubmit()) {
            if(this.Submit2IsPressed == false ){
                submitScore(usernamePlayerTwo.getText(), p2score);
                this.Submit2IsPressed = true;
            }
            CoronaWars.getInstance().batch.draw(highscoreSavedText, Gdx.graphics.getWidth()*3/4-highscoreSavedText.getWidth(), Gdx.graphics.getHeight()-highscoreSavedText.getHeight()-450);
        }

        CoronaWars.getInstance().batch.end();

        stage.act();
        stage.draw();
    }



    public void submitScore(String name, int score){
        CoronaWars.getInstance()._FBC.SetNewHighscore(name, score);
    }




    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlasText.dispose();
        textureAtlasButtons.dispose();
        textureAtlasDraw.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
