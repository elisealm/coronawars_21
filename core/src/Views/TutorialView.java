package Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.CoronaWars;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;

import javax.swing.JScrollPane;

import Controllers.TutorialControler;
import Models.TutorialModel;


import Controllers.Buttons.ChangeViewButton;
import Singleton.BackgroundColor;

public class TutorialView implements Screen {

    private TextureAtlas textureAtlasTitle;
    private TextureAtlas textureAtlasButtons;
    private Skin skin;

    private final BackgroundColor backgroundColor;
    private Sprite mainMenu;
    private Sprite title;
    private ChangeViewButton mainmenuScreenButton;
    private Stage stage;
    private Texture img1, img2,  img3,  img4, img5, img6, img7, img8, img9, img10;
    private TutorialControler tutorial;


    public TutorialView() {
        super();
        this.tutorial = new TutorialControler();
        this.skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
        this.img1 = new Texture(Gdx.files.internal("tutorial/pre_game_countdown_img.png"));
        this.img2 = new Texture(Gdx.files.internal("tutorial/move_buttons_img.png"));
        this.img3 = new Texture(Gdx.files.internal("tutorial/shoot_buttons_img.png"));
        this.img4 = new Texture(Gdx.files.internal("tutorial/target_img.png"));
        this.img5 = new Texture(Gdx.files.internal("tutorial/stats_info_img.png"));
        this.img6 = new Texture(Gdx.files.internal("tutorial/powerups_img.png"));
        this.img7 = new Texture(Gdx.files.internal("tutorial/quit_game_img.png"));
        this.img8 = new Texture(Gdx.files.internal("tutorial/add_highscore_names_img.png"));
        this.img9 = new Texture(Gdx.files.internal("tutorial/add_highscore_saved_img.png"));
        this.img10 = new Texture(Gdx.files.internal("tutorial/highscore_added_to_table_img.png"));
        this.textureAtlasButtons = new TextureAtlas("logo_buttons.txt");
        this.textureAtlasTitle = new TextureAtlas("settings_highscore_sprites.txt");
        this.backgroundColor = BackgroundColor.getInstance();
        this.stage = new Stage();
        final ScrollPane scrollpane = new ScrollPane(tutorial.getTutorial().getTable(), skin);

        tutorial.getTutorial().getContainer().setFillParent(true);
        scrollpane.setFlickScroll(true);
        scrollpane.setScrollingDisabled(false,true);
        scrollpane.setFadeScrollBars(false);
        tutorial.getTutorial().getContainer().add(scrollpane);
        tutorial.getTutorial().getContainer().columnDefaults(0).spaceLeft(600);
        tutorial.getTutorial().getContainer().columnDefaults(6).spaceRight(600);
        tutorial.getTutorial().getTable().center().defaults().space(650);
        scrollpane.setSize(1200,380);

        this.title = textureAtlasTitle.createSprite("scrollthroughtutorial");

        mainMenu = textureAtlasButtons.createSprite("mainmenu2x");
        mainmenuScreenButton = new ChangeViewButton(mainMenu, "MENU", Gdx.graphics.getWidth()/8 - mainMenu.getWidth()/4, 0);
        stage.addActor(mainmenuScreenButton.getButton());

        tutorial.getTutorial().addImage(img1);
        tutorial.getTutorial().addImage(img2);
        tutorial.getTutorial().addImage(img3);
        tutorial.getTutorial().addImage(img4);
        tutorial.getTutorial().addImage(img5);
        tutorial.getTutorial().addImage(img6);
        tutorial.getTutorial().addImage(img7);
        tutorial.getTutorial().addImage(img8);
        tutorial.getTutorial().addImage(img9);
        tutorial.getTutorial().addImage(img10);


        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void show() {
        stage.addActor(tutorial.getTutorial().getContainer());
        tutorial.getTutorial().getContainer();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        CoronaWars.getInstance().batch.begin();
        CoronaWars.getInstance().batch.draw(title, Gdx.graphics.getWidth()/2-title.getWidth()/2, Gdx.graphics.getHeight()-130);
        CoronaWars.getInstance().batch.end();

        stage.act();
        stage.draw();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
