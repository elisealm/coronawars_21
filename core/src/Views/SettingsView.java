package Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.CoronaWars;

import Controllers.Buttons.ChangeViewButton;
import Controllers.Buttons.ChangeBackgroundButton;
import Controllers.Buttons.SoundOnOffButton;
import Controllers.SettingsController;
import Singleton.BackgroundColor;
import Singleton.SoundSingleton;

public class SettingsView implements Screen {
    private ChangeViewButton mainmenuScreenButton;
    private TextureAtlas textureAtlas;
    private TextureAtlas textureAtlasTitle;

    private Sprite mainMenu;
    private Sprite title;
    private Sprite brown;
    private Sprite brownChecked;
    private Sprite purple;
    private Sprite purpleChecked;
    private Sprite green;
    private Sprite greenChecked;
    private Sprite sound;
    private Sprite background;
    private Sprite soundOn;
    private Sprite soundOff;
    private Stage stage;

    private ChangeBackgroundButton brownButton;
    private ChangeBackgroundButton purpleButton;
    private ChangeBackgroundButton greenButton;

    private SoundOnOffButton soundOnButton;
    private SoundOnOffButton soundOffButton;

    private BackgroundColor backgroundColor;
    private SoundSingleton sound1;

    private SettingsController settings;


    public SettingsView(){

        this.stage = new Stage();
        this.settings = new SettingsController();

        this.backgroundColor = BackgroundColor.getInstance();
        this.sound1 = sound1.getInstance();

        this.textureAtlas = new TextureAtlas("logo_buttons.txt");
        this.textureAtlasTitle = new TextureAtlas("settings_highscore_sprites.txt");

        this.title = textureAtlasTitle.createSprite("settings");
        this.sound = textureAtlasTitle.createSprite("sound");
        this.background = textureAtlasTitle.createSprite("background");

        this.mainMenu = textureAtlas.createSprite("mainmenu2x");
        this.mainmenuScreenButton = new ChangeViewButton(mainMenu, "MENU", Gdx.graphics.getWidth()/2 - mainMenu.getWidth()/2, 250);
        stage.addActor(mainmenuScreenButton.getButton());

        this.brown = textureAtlasTitle.createSprite("brown");
        this.brownChecked = textureAtlasTitle.createSprite("brownchecked");
        this.purple = textureAtlasTitle.createSprite("purple");
        this.purpleChecked= textureAtlasTitle.createSprite("purplechecked");
        this.green = textureAtlasTitle.createSprite("green");
        this.greenChecked = textureAtlasTitle.createSprite("greenchecked");

        this.soundOn = textureAtlasTitle.createSprite("soundon");
        this.soundOff = textureAtlasTitle.createSprite("soundoff");

        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render(float delta) {
        //set background color
        Gdx.gl.glClearColor(backgroundColor.redNumber/255f, backgroundColor.greenNumber/255f, backgroundColor.blueNumber/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //determine which sound button to show
        if(sound1.sound){
            soundOnButton = new SoundOnOffButton(soundOn, Gdx.graphics.getWidth()-500,500, settings);
            stage.addActor(soundOnButton.getButton());
        }else if (sound1.sound == false){
            soundOffButton = new SoundOnOffButton(soundOff, Gdx.graphics.getWidth()-500, 500, settings);
            stage.addActor(soundOffButton.getButton());
        }

        //chose color button to show
        if (backgroundColor.redNumber==170) {
            brownButton = new ChangeBackgroundButton(brownChecked, 100, 500, "brown", settings);
            stage.addActor(brownButton.getButton());
            purpleButton = new ChangeBackgroundButton(purple, 300, 500, "purple", settings);
            stage.addActor(purpleButton.getButton());
            greenButton = new ChangeBackgroundButton(green, 500, 500, "green", settings);
            stage.addActor(greenButton.getButton());
        }else if (backgroundColor.redNumber==176){
            brownButton = new ChangeBackgroundButton(brown,  100, 500, "brown", settings);
            stage.addActor(brownButton.getButton());
            purpleButton = new ChangeBackgroundButton(purpleChecked,  300, 500, "purple", settings);
            stage.addActor(purpleButton.getButton());
            greenButton = new ChangeBackgroundButton(green,  500, 500, "green", settings);
            stage.addActor(greenButton.getButton());
        }else if (backgroundColor.redNumber==184) {
            brownButton = new ChangeBackgroundButton(brown, 100, 500, "brown", settings);
            stage.addActor(brownButton.getButton());
            purpleButton = new ChangeBackgroundButton(purple, 300, 500, "purple", settings);
            stage.addActor(purpleButton.getButton());
            greenButton = new ChangeBackgroundButton(greenChecked, 500, 500, "green", settings);
            stage.addActor(greenButton.getButton());
        }

        CoronaWars.getInstance().batch.begin();
        CoronaWars.getInstance().batch.draw(title, Gdx.graphics.getWidth()/2-title.getWidth()/2, Gdx.graphics.getHeight()-500);
        CoronaWars.getInstance().batch.draw(background,300+(background.getWidth()/2)-background.getWidth()/2,700); //background color
        CoronaWars.getInstance().batch.draw(sound,Gdx.graphics.getWidth()-500+20,700);
        CoronaWars.getInstance().batch.end();
        stage.act();
        stage.draw();
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
        textureAtlasTitle.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }
}
