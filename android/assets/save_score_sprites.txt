save_score_sprites.png
size: 509, 685
format: RGBA8888
filter: Linear,Linear
repeat: none
addhighscore
  rotate: false
  xy: 0, 510
  size: 509, 175
  orig: 509, 175
  offset: 0, 0
  index: -1
highscore_saved
  rotate: false
  xy: 0, 0
  size: 229, 42
  orig: 229, 42
  offset: 0, 0
  index: -1
player1
  rotate: false
  xy: 0, 56
  size: 142, 57
  orig: 142, 57
  offset: 0, 0
  index: -1
player1name
  rotate: false
  xy: 142, 56
  size: 258, 57
  orig: 258, 57
  offset: 0, 0
  index: -1
player1score
  rotate: false
  xy: 0, 113
  size: 264, 57
  orig: 264, 57
  offset: 0, 0
  index: -1
player1won
  rotate: false
  xy: 0, 284
  size: 494, 113
  orig: 494, 113
  offset: 0, 0
  index: -1
player2
  rotate: false
  xy: 264, 113
  size: 145, 57
  orig: 145, 57
  offset: 0, 0
  index: -1
player2name
  rotate: false
  xy: 0, 170
  size: 261, 57
  orig: 261, 57
  offset: 0, 0
  index: -1
player2score
  rotate: false
  xy: 229, 0
  size: 267, 56
  orig: 267, 56
  offset: 0, 0
  index: -1
player2won
  rotate: false
  xy: 0, 397
  size: 500, 113
  orig: 500, 113
  offset: 0, 0
  index: -1
score
  rotate: false
  xy: 261, 170
  size: 118, 57
  orig: 118, 57
  offset: 0, 0
  index: -1
timeleft
  rotate: false
  xy: 0, 227
  size: 167, 57
  orig: 167, 57
  offset: 0, 0
  index: -1
