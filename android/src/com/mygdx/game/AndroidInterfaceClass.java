package com.mygdx.game;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Models.HighscoreHolder;

import static android.content.ContentValues.TAG;


public class AndroidInterfaceClass implements IDatabase{

    FirebaseDatabase database;
    DatabaseReference myRef;


    public AndroidInterfaceClass() {
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void SetHighscoreListener(final HighscoreHolder highscoreHolder) {
        myRef = database.getReference().child("Scores");
        // Get highscores from database, in ascending order
        myRef.orderByValue().addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // set scores to the retrieved result from database (will be sorted in ascending order)
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    highscoreHolder.setNewScore(userSnapshot.getKey(), userSnapshot.getValue(Long.class));
                }

                Log.d(TAG, "Scores " + highscoreHolder.getScoresDescending() );
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }

    @Override
    public void SetNewHighscore(String name, int score) {
        myRef = database.getReference("Scores").child(name);
        myRef.setValue(score);
    }







}
